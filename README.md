# GitLab CI Pipeline for Sociomantic/Dunnhumby D projects

This repository holds a template for [GitLab
CI](https://docs.gitlab.com/ee/ci/) pipeline for D Programming Language
projects that follow the Sociomantic/Dunnhumby D standards.

The provided `gitlab-ci.yml` file define several job templates that can be used
to build CI pipelines. Each job supports different features and can be
configured through environment variables. It also define a set of default
`stages` and configuration `variables`, as well as some pre-defined jobs that
are very likely used always.


## Predefined `stages`

The following stages are predefined:

* `build-test`: Building and testing are not separated as it will make using
  the template more complicated with very little gain.
* `deploy`: Everything that is meant to publish any *artifact* goes here.

You can change these predefined `stages` by defining your own stages in your
job, but you should keep at lest both of these, and also keep the order. For
example:

```yaml
stages:
  - prepare
  - build-test
  - notify
  - deploy
  - party
```

## Basic usage

You can easily use this pipeline template by using a *remote include*:

```yaml
include:
  - project: sociomantic-test/gitlab-ci-dlang
    file: gitlab-ci.yml
    ref: v0.x.x
```

Of course you should adjust the ref to the branch or tag that suits your need.

Then you can use the provided job templates (as described above) to create your
own jobs to build, test, build and deploy packages, generate and publish
documentation, etc.

You can also take a look at the `examples/` directory for usage examples.


## Predefined general purpose `variables`

The following default variables are defined:

* `COV`: `"0"`. Tells *MakD* not to generated coverage reports. See
  `.dlang-build-test` job template for details.
* `V`: `""`. Tells *MakD* to not be verbose.

Each job normally use other variables to control its behaviour and set
configuration. Please look at the specific job templates for details.


## Abstract job templates

The following job templates are used by most job templates provided here, and
define common behaviour and features.

### `.dlang-image`

This job template defines only the *Docker* image to be used by the job. It
uses the `sociomantictsunami/develdlang:$DIST-$CACHALOT_TAG` image and it
changes the *entrypoint* so the `cachalot` user is used internally when running
commands.

#### Used variables

* `DIST`: *Ubuntu* distribution to use for the *cachalot* image tag. Example: `xenial`.
* `CACHALOT_TAG`: Specific *Cachalot* tag for the particular image. Example: `v7`.

Please consult *Cachalot* documentation for more information on how tags are named.


### `.dlang-common`

```yaml
extends: .dlang-image
```

This job template defines a `before_script` to prepare the *Cachalot* image
environment to match what the current job/project needs, installing the DMD
compiler, etc.

The following actions are performed by this `before_script`:

1. Print debugging information, like environment variables and user ID.

2. Fix permissions so all files are owned by the `cachalot` user.

3. `sudo apt update`

4. Install the DMD compiler according to the `$DMD` environment variable:

   * If DMD starts with `dmd`, it will install nothing, assuming the default
     installed packages will be used.
   * If DMD is `1.*` it will install `dmd1=$DMD-$DIST`.
   * If DMD is` 2.*.s*` it will install `dmd-transitional=$DMD-$DIST`.
   * If DMD is `2.*` it will install `dmd-compiler=$DMD` or `dmd-bin=$DMD`
     depending on `$DMD` (to accomodate to a package name change) and some
     extra packages, like `libphobos2-dev` and `dmd-tools`.
   * If DMD has another shape, it will error.

5. Run the script `docker/build` if it is present in the repo and it is
   executable.

6. Set the `DC` and `DVER` environment variables based on the `DMD` environment
   variable, if present. The `DMD` variable is expected to hold the DMD version
   to use:
   
   * For `DMD` `1.x`, `DC` will be set to `dmd1` and `DVER` to `1`.
   * For `DMD` `2.x.y.sN`, `DC` will be set to `dmd-transitional` and `DVER` to
     `2`.
   * For `DMD` `2.x.y`, `DC` will be set to `dmd` and `DVER` to `2`.
   
   It errors if `DMD` is not set.

7. Set the `D2_ONLY` variable if the file `.D2-ready` exists and it has a line
   that matches `^ONLY$`.

8. Set the variable `version` with *MakD*'s calculated version (using
   `mkversion.sh`). This is to avoid having a `dirty` flag on the version if
   D2-conversion is performed. In this case the version calculated previous to
   run `d1to2fix` is used so the version string is not *dirtied*.

9. Print Debugging information about the set variables and compiler being used.

10. Convert the code to D2 using `make d2conv` if `$D2_ONLY` is not `false` and
    `$DVER` is `2`.

#### Used variables

* Variables used by `.dlang-image`.
* `DMD`: DMD version to use by the job and package to install.
* `DIST`: Used to calculate the version of the DMD package to install.
* `D2_ONLY` / `DVER` are used to determine if the code needs to be converted to
  D2.

#### Provided variables

* `DC`: D compiler to use by the job.
* `DVER`: D version being used: `1` or `2` depending on the compiler to use.
* `D2_ONLY`: The project is D2-only (`true`) or not (`false`).
* `version`: Version of the repository as calculated by *MakD* before the repo
  is dirtied (if it is).


### `.dlang-deploy-pages`

```yaml
image: ubuntu
stage: deploy
```

This job is not exactly *abstract* but is not intended to be used directly by
the user, unless it is hacking the templates. It is used by the pre-defined job
`pages` to deploy the documentation to *GitLab Pages*. The documentation is
normally built by `.dlang-build-docs`. And this job just creates a `index.html` for the general documentation.

**KNOWN BUGS:**

This job is pretty much flawed for now, as GitLab Pages will
overwrite the current published files, this only publishes the last built
documentation, not matter if it is from an old branch or a new tag. The
intention was to have documentation for all versions published, but that will
need some other static pages server, or using another git repo as an
intermediary.

**NOTE:**

This job will **not** run for tags ending with `+d2`, as they are automatic D2
releases, and packages were already built by the job that did the automatic
release.

**`artifacts:`** `public/index.html` and `public/api/index.html`.



## Concrete job templates

These are job templates that are meant to be used just to define your jobs in
your CI pipeline. Normally you just need to `extends` from them, maybe override
a few `variables` and that's it.

### `.dlang-build-test`

```yaml
extends: .dlang-common
stage: build-test
```

This job is intended to be extended directly by the user to create jobs to
build and test D projects. One job should be created per *environment* to test,
so normally you want to extend from it and override some `variables`.

Steps performed by this job:

1. Build using `make all`.

2. Test using `make test` generating JUnit unittest reports.

3. Send code coverage reports to *Codecov* if `$COV` is `1` and
   `$CODECOV_TOKEN` is defined.

**`artifacts:`** JUnit reports.

**NOTE:**

This job will **not** run if a D1 DMD compiler is used and a tag ending with
`+d2` is being built, so users don't have to worry about handling D1 builds for
automatically converted D2 releases. For more details about automatic D2
releases, consult the documentation for `.dlang-release-d2`.


#### Used variables

* Variables used by `.dlang-common`.
* `DC` is used by *MakD* as the D compiler to use to build the project.
* `COV` / `CODECOV_TOKEN`: `COV` is used by *MakD* to see if code coverage
  reports must be generated, and this job uploads them to *Codecov* if
  `CODECOV_TOKEN` is defined.
* Many other environment variables are used by *MakD*, please consult its
  documentation for details.


### `.dlang-build-pkg`

```yaml
extends: .dlang-common
stage: build-test
```

This job is intended to be extended directly by the user to create jobs to
build Debian packages for the project. One job should be created per
*environment* to create a package from, usually one per D1/D2 compiler. So
normally you want to extend from it and override some `variables`.

The job builds the package using `make pkg "VERSION=$version"
PKG_SUFFIX=-d$DVER`. This means packages will have a `-d1`/`-d2` suffix
depending on the D version used to build it and the version will not be dirtied
by the conversion to D2.

Normally the packages are deployed/published via the job template
`.dlang-deploy-packages`.

**`artifacts:`** Built packages are archived in `build/pkg`.

**NOTE:**

This job will **not** run for tags ending with `+d2` because these are
automatic releases, and packages were already built by the job that did the
automatic release.

#### Used variables

* Variables used by `.dlang-common`.
* `version` is used as the package version.
* `DC` is used by *MakD* as the D compiler to use to build the project.
* `DVER` is used to set the package suffix.
* Many other environment variables are used by *MakD*, please consult its
  documentation for details.


### `.dlang-check-closures`

```yaml
extends: .dlang-common
stage: build-test
```

This job is intended to be extended directly by the user to create a job to
check if the project is using closure (delegates allocated in the heap). If
closures are used this job will fail. This should be used only with D2
compilers, as D1 doesn't have closures.

Steps performed by this job:

1. Build using `make all`.

2. Test if closures are using via `make fasttest` passing the `-vgc` to the
   compiler an checking that the output doesn't contain the word `closure`.

#### Used variables

* Variables used by `.dlang-common`.
* `DC` is used by *MakD* as the D compiler to use to build the project.
* Many other environment variables are used by *MakD*, please consult its
  documentation for details.


### `.dlang-build-docs`

```yaml
extends: .dlang-common
stage: build-test
```

This job is intended to be extended directly by the user to create jobs to
generate the project's documentation. The documentation is generated via `make
doc`. Documentation is normally deployed/published via the
`.dlang-deploy-pages` job and the GitLab-standard, pre-defined by this CI
pipeline, `pages` job.

**`artifacts:`** Generated documentation is archived in
`public/api/$CI_COMMIT_REF_SLUG-D$DVER`.

**NOTE:**

This job will **not** run for tags ending with `+d2` because these are
automatic releases, and documentation was already built by the job that did the
automatic release.

#### Used variables

* Variables used by `.dlang-common`.
* `DVER` is used as the documentation directory suffix.


### `.dlang-deploy-packages`

```yaml
extends: .dlang-image
stage: deploy
```

This job is intended to be extended directly by the user to create a job to
deploy the generated packages (normally built by `.dlang-build-pkg`). The
packages are uploaded to *Bintray*, for which the `DGLCI_BINTRAY_USER` and
`DGLCI_BINTRAY_KEY` should be defined, normally in the GitLab project
**Settings**, under **CI / CD**, **Variables** section.

Packages will be uploaded using the `jfrog` tool in the *Bintray* repository by
the `DGLCI_BINTRAY_REPO` variable, which should have the form
`ORG/REPO/PACKAGE`.  Please consult *Bintray* documentation for more detail.

Projects are expected to follow *SemVer*. If the tag contains a `-` character,
it is considered a pre-release and packages will be uploaded to the
`prerelease` *component*, otherwise they will be uploaded to the `release`
*component* (this can be overridden, see *Used variables* for details).

To upload packages, a *Bintray* version is created first, using the tag name
and description. A few extra variables can be used to configure how to upload
packages. See *Used variables* for details.

**NOTE:**

This job will run only on tags and when the `DGLCI_BINTRAY_REPO` variable is
defined. Also this job will **not** run for tags ending with `+d2` because
these are automatic releases, and packages were already published by the job
that did the automatic release.

#### Used variables

* Variables used by `.dlang-image`.
* `DIST` is used to determine which *distribution* to use to upload the
  packages.
* `CI_COMMIT_TAG` is used to get the tag to create the new *Bintray* version.
* `DGLCI_BINTRAY_DRY_RUN` is used for debugging purposes. If is set to `true`,
  packages will not be uploaded but instead the `jfrog` commands that would
  have been used are printed to stdout.
* `DGLCI_BINTRAY_USER` and `DGLCI_BINTRAY_KEY` are used to authenticate in
  *Bintray*.
* `DGLCI_BINTRAY_REPO` defines where to upload the packages.
* `DGLCI_BINTRAY_COMP_RELEASE` and `DGLCI_BINTRAY_COMP_PRERELEASE` define the
  repository *component* to use when uploading. By default they are `release`
  and `prerelease` respectively.
* `DGLCI_BINTRAY_PUBLISH` tells *Bintray* if the uploaded packages should be
  published (`true` by default).
* `DGLCI_BINTRAY_OVERRIDE` is used to determine if pre-existing packages should
  be overwritten (`false` by default, will error if there are pre-existing
  packages with the same name and version).

This jobs references many terms that come from *Bintray*, please consult
*Bintray*'s documentation for details.

#### Used artifacts

* Everything in `build/pkg/*.deb` will be uploaded. These normally come from
  the job template `.dlang-build-pkg`.


### `.dlang-release-d2`

```yaml
extends: .dlang-common
stage: deploy
```

This job is intended to be extended directly by the user to create a job to
convert the project to D2 and do an automatic D2 release.

Steps performed by this job:

1. Mark the project as D2-only by creating the file `.D2-ready` with `ONLY` as
   contents.

2. Convert the code to D2 via `make d2conv`.

3. Convert the appropriate submodules (defined via
   `DLANG_D2_RELEASE_SUBMODULES`) to D2 by checking out the current tag with
   `+d2` as suffix. If the tag with the suffix doesn't exist it will error.

4. Commit all the changes using the same user as the last commit/tag author.

5. Create a new annotated tag by appending `+d2` to the current tag name.

6. Push the new tag to the project's Git repository using `DGLCI_GITLAB_USER`
   and `DGLCI_GITLAB_OAUTH_TOKEN` for credentials.

7. Create a new GitLab release via the API.

The credential should be configured via the project's **Settings**, under **CI
/ CD**, **Variables** section.


**NOTE:**

This job runs only for tags and will **not** run for tags ending with `+d2`
because these are automatic releases, and they already created by this very
same job when the original tag was built.

#### Used variables

* Variables used by `.dlang-common`.
* `DGLCI_GITLAB_USER` and `DGLCI_GITLAB_OAUTH_TOKEN` as GitLab credentials
  while pushing to the repo and to create the release via the API. The token
  should have write access to the repo and be able to use the API.
* `DLANG_D2_RELEASE_SUBMODULES` to specify which submodules should be converted
  to D2.
* `CI_COMMIT_REF_NAME` to get the tag name.


## Pre-defined jobs

These are actual jobs, that will be automatically created for the user when
including this pipeline template.

### `pages`

This is a standard job used to publish pages to *GitLab Pages*, it need to be
named this way and it only extends the `.dlang-deploy-pages` without overriding
anything. Please consult GitLab Pages documentation for details.

